package com.hamed.messager.data.repository

import com.hamed.messager.data.source.AppDataStore
import com.hamed.messager.domain.Repository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class RepositoryImpl @Inject constructor(
    private val appDataStore: AppDataStore
) : Repository {

    override suspend fun submitPhone(phone: String) {
        appDataStore.setPhone(phone)
    }

    override fun getPhoneNumber(): Flow<String> = appDataStore.getPhone()
}