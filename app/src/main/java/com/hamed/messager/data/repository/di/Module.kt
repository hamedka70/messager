package com.hamed.messager.data.repository.di

import com.hamed.messager.data.repository.RepositoryImpl
import com.hamed.messager.domain.Repository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
abstract class Module {
    @Binds
    abstract fun bindRepository(repositoryImpl: RepositoryImpl): Repository
}