package com.hamed.messager.data.source

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.mapNotNull
import javax.inject.Inject

internal val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "app")

interface AppDataStore {
    suspend fun setPhone(phoneNumber: String)
    fun getPhone(): Flow<String>
}


class AppDataStoreImpl @Inject constructor(
    @ApplicationContext private val context: Context
) : AppDataStore {

    companion object {
        private val PHONE_NUMBER = stringPreferencesKey("phone-number")
    }

    override suspend fun setPhone(phoneNumber: String) {
        context.dataStore.edit {
            it[PHONE_NUMBER] = phoneNumber
        }
    }

    override fun getPhone(): Flow<String> =
        context.dataStore.data.mapNotNull {
            it[PHONE_NUMBER]
        }

}
