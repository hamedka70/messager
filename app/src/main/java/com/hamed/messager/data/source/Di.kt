package com.hamed.messager.data.source

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent


@Module
@InstallIn(SingletonComponent::class)
abstract class Module {
    @Binds
    abstract fun bindDataStore(dataStoreImpl: AppDataStoreImpl): AppDataStore
}