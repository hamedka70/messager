package com.hamed.messager.domain

import kotlinx.coroutines.flow.Flow


interface Repository {
    suspend fun submitPhone(phone: String)
    fun getPhoneNumber(): Flow<String>
}