package com.hamed.messager.presenter.ui.screens

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.telephony.SmsManager
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.hamed.messager.presenter.viewModels.SendMessageViewModel
import com.hamed.messager.presenter.viewModels.SubmitPhoneViewModel

@Composable
fun MessageNavigation(
    modifier: Modifier,
    context: Context
) {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = "home") {
        composable("home") {
            MainScreen(
                sendMessageCallback = {
                    navController.navigate("sendMessage")
                },
                setPhoneCallback = {
                    navController.navigate("setPhoneNumber")
                }
            )
        }
        composable(
            "sendMessage"
        ) {
            val viewmodel = hiltViewModel<SendMessageViewModel>()
            SmsSenderPreview(
                phoneState = viewmodel.phoneState,
                messageState = viewmodel.messageState
            ) { phone: String, message: String ->
                val sentPI: PendingIntent = PendingIntent.getBroadcast(
                    context,
                    0,
                    Intent("SMS_SENT"),
                    PendingIntent.FLAG_IMMUTABLE
                )
                context.getSystemService(SmsManager::class.java).sendTextMessage(
                    phone,
                    null,
                    message,
                    sentPI,
                    null
                )
            }
        }

        composable(
            "setPhoneNumber"
        ) {
            val viewmodel = hiltViewModel<SubmitPhoneViewModel>()
            SubmitPhonePreview(viewmodel.phoneState) {
                viewmodel.submitPhone(it)
            }
        }
    }
}
