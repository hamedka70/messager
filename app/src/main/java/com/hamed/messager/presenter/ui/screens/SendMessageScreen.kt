package com.hamed.messager.presenter.ui.screens


import android.widget.Toast
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.KeyboardType

import androidx.compose.ui.unit.dp
import com.hamed.messager.R
import kotlinx.coroutines.flow.MutableStateFlow


@Composable
fun SmsSenderPreview(
    phoneState: MutableStateFlow<String>,
    messageState: MutableStateFlow<String>,
    sendClick: (phone: String, message: String) -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
    ) {
        val phoneInput by phoneState.collectAsState()
        TextField(
            modifier = Modifier.fillMaxWidth(),
            value = phoneInput,
            onValueChange = { phoneState.value = it },
            keyboardOptions = KeyboardOptions.Default.copy(
                keyboardType = KeyboardType.Phone
            ),
            label = {
                Text(text = "Phone Number")
            }
        )

        Spacer(modifier = Modifier.height(24.dp))

        val smsInput by messageState.collectAsState()

        var showToast by remember {
            mutableStateOf(false)
        }

        TextField(
            modifier = Modifier.fillMaxWidth(),
            value = smsInput,
            onValueChange = { messageState.value = it },
            label = {
                Text(text = "Message")
            })
        Spacer(modifier = Modifier.height(48.dp))

        IconButton(
            onClick = {
                sendClick(phoneInput, smsInput)
                phoneState.value = ""
                messageState.value =""
                showToast = true
            }) {
            Icon(
                painter = painterResource(id = R.drawable.send),
                contentDescription = "sendBTN",
                tint = Color.Unspecified
            )

        }

        if (showToast){
            Toast.makeText(LocalContext.current, "Message sent", Toast.LENGTH_SHORT).show()
            showToast = false
        }

    }
}

