package com.hamed.messager.presenter.ui.screens

import android.widget.Toast
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.flow.MutableStateFlow


@Composable
fun SubmitPhonePreview(
    phoneState: MutableStateFlow<String>,
    submitPhone: (String) -> Unit
) {
    val phoneText by phoneState.collectAsState()

    var showToast by remember {
        mutableStateOf(false)
    }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(24.dp)
    ) {

        TextField(
            modifier = Modifier.fillMaxWidth(),
            value = phoneText,
            onValueChange = {
                phoneState.value = it
            },
            keyboardOptions = KeyboardOptions.Default.copy(
                keyboardType = KeyboardType.Phone
            ),
            label = {
                Text(text = "Phone Number")
            })

        Spacer(modifier = Modifier.height(24.dp))

        Button(onClick = {
            submitPhone(phoneText)
            showToast = true
            phoneState.value = ""
        }) {
            Text(text = "Submit")
        }

        if (showToast){
            Toast.makeText(LocalContext.current, "Phone Submit Successful", Toast.LENGTH_SHORT).show()
            showToast = false
        }

    }
}