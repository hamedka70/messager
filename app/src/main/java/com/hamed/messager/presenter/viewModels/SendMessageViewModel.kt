package com.hamed.messager.presenter.viewModels

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import javax.inject.Inject

@HiltViewModel
class SendMessageViewModel @Inject constructor(): ViewModel() {
    val messageState = MutableStateFlow("")
    val phoneState = MutableStateFlow("")
}