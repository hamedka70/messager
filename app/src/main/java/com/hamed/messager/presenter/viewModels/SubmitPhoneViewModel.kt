package com.hamed.messager.presenter.viewModels


import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hamed.messager.domain.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class SubmitPhoneViewModel @Inject constructor(private val repository: Repository): ViewModel() {

    val getPhone = repository.getPhoneNumber()

    val phoneState = MutableStateFlow("")

    fun submitPhone(phone: String)= viewModelScope.launch {
        repository.submitPhone(phone)
    }


}