package com.hamed.messager.util

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import com.hamed.messager.R
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SmsNotification @Inject constructor(@ApplicationContext private val context: Context) {
    private val notificationManager: NotificationManager =
        context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    companion object {
        lateinit var instace: SmsNotification
        const val NOTIFICATION_CHANNEL_ID = "messager notification"
    }

    init {
        instace = this
    }

    private val name = "Massager"
    private val descriptionText = "Message Received"
    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(NOTIFICATION_CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }
            notificationManager.createNotificationChannel(channel)
        }
    }

    fun create(
        contentTitle: String,
        contentText: String?,
    ): Notification {

        val builder: NotificationCompat.Builder =
            NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                .setChannelId(NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setOngoing(false)
                .setAutoCancel(false)
                .setSound(null)
                .setForegroundServiceBehavior(NotificationCompat.FOREGROUND_SERVICE_IMMEDIATE)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setStyle(NotificationCompat.BigTextStyle())
        builder
            .setContentTitle(contentTitle)
            .setContentText(contentText)
        return builder.build().let {
            createNotificationChannel()
            it
        }
    }

}