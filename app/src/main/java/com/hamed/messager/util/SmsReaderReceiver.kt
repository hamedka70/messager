package com.hamed.messager.util

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.provider.Telephony
import android.util.Log


class SmsReaderReceiver(
    val smsCallback: (smsSender: String , smsText: String)-> Unit
): BroadcastReceiver() {

    private var phoneNumber : String = "0"
    override fun onReceive(p0: Context?, p1: Intent?) {
        for (sms in Telephony.Sms.Intents.getMessagesFromIntent(
            p1
        )) {
            val smsSender = sms.originatingAddress
            val smsMessageBody = sms.displayMessageBody
            Log.e("sms" , " a SmsReaderReceiver")

            if (smsSender == phoneNumber ||  smsSender == "+98${phoneNumber.substring(1)}") {
                smsCallback(smsSender , smsMessageBody)
                break
            }
        }
    }

    fun setPhoneNumber(phone: String){
        phoneNumber = phone
    }
}