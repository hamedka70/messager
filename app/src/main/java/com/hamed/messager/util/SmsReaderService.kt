package com.hamed.messager.util


import android.annotation.SuppressLint
import android.content.IntentFilter
import android.content.pm.ServiceInfo
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.ServiceCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.lifecycleScope
import com.hamed.messager.domain.Repository
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import javax.inject.Inject

@SuppressLint("ForegroundServiceType")
@AndroidEntryPoint
class SmsReaderService : LifecycleService() {

    private var smsReaderReceiver: SmsReaderReceiver? = null

    @Inject
    lateinit var repository: Repository
    @Inject
    lateinit var notification: SmsNotification

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onCreate() {
        super.onCreate()


        smsReaderReceiver =
            SmsReaderReceiver { smsSender, smsText ->
                ServiceCompat.startForeground(
                    this,
                    25,
                    notification.create(
                        smsSender,
                        smsText,
                    ),
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        ServiceInfo.FOREGROUND_SERVICE_TYPE_REMOTE_MESSAGING
                    } else {
                        0
                    }
                )
            }

        ContextCompat.registerReceiver(
            this,
            smsReaderReceiver,
            IntentFilter("android.provider.Telephony.SMS_RECEIVED"),
            ContextCompat.RECEIVER_EXPORTED
        )
        lifecycleScope.launch {
            repository.getPhoneNumber().stateIn(this).collectLatest {
                smsReaderReceiver?.setPhoneNumber(it)
            }
        }

    }

    override fun onDestroy() {
        unregisterReceiver(smsReaderReceiver)
        super.onDestroy()
    }

}